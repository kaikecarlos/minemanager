<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function list() {
        $result = User::All();
        return view('pages/UserList')->withUsers($result);
    }
}
