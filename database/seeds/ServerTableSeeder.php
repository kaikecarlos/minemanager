<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ServerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servers')->insert([
            'ip' => 'mc.sample.com',
            'build' => 'git-Spigot-2086bb0-d0a3620 (MC: 1.12.2)',
            'name' => 'Sample Server',
            'total_jogadores' => 0,
            'banned_players' => 0,
            'plugin_size' => 0,
            'discord_id' => 0
        ]);
    }
}
