@extends('layouts/app')
@section('content')
funcicoandasodnasidsdisjdi
    <div class="card m-0 mt-5">
        <div class="d-flex">
            <h5>Registrar um novo servidor</h5>
        </div>
        <div class="card-body">
                        <h3 class="card-title m-t-15">Server Info</h3>
                        <hr>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                        <label class="control-label">IP</label>
                                        <input type="text" id="server_ip" class="form-control" placeholder="127.0.0.1">
                                        <small class="form-control-feedback"> O IP pode ser numerico ou não </small> </div>
                                <div class="col-md-6">
                                        <label class="control-label">Nome</label>
                                        <input type="text" id="server_name" class="form-control form-control-danger" placeholder="Sample Server">
                            </div>
                                <div class="actions ml-3">
                                    <button class="btn btn-success" id="confirm" onclick="save();"> <i class="fa fa-check"></i> Criar</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function save() {
            sIP = $('#server_ip').val();
            sName = $('#server_name').val();
            $.get('/servers/save/', { ip: sIP, name: sName}, (data, status) =>{
                if(status == 'success') {
                    swal("Tudo Certo!", "Você registrou o servidor com sucesso!", "success");
                }
            });
        }
    </script>
@endsection