@extends('layouts/app')
@section('content')
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <div>
                        <h4 class="card-title"><span class="lstick"></span>Lista de Servidores</h4></div>
                    </div>
                    <div class="table-responsive">
                        <table class="table vm no-th-brd" id="myTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Servidor</th>
                                    <th>IP</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($servers as $server)
                                    <tr>
                                        <td>
                                            <h6>{{$server->id}}</h6>
                                        </td>
                                        <td>
                                            <h6>{{$server->name}}</h6>
                                        </td>
                                        <td>
                                            <h6>{{$server->ip}}</h6>
                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
@endsection
@section('scripts')
    <script src="{{asset('js/lib/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js')}}"></script> 
    <script>
        $(document).ready(() => {
            $('#myTable').DataTable();
        });
    </script>

@endsection