@extends('layouts/app')
@section('content')
	
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Dashboard</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
				<li class="breadcrumb-item">Apps</li>
				<li class="breadcrumb-item active">{{$server->name}}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2">
			<div class="card">
				<div class="card-body">
					<div class="m-r-20 align-self-center"><span><i class="fa fa-user f-s-40 m-r-20 color-info"></i></span></div>
					<div class="align-self-center">
						<h6 class="text-muted m-t-0 m-b-0">Total de Jogadores</h6>
						<h4 class="m-t-0">{{$server->total_jogadores}}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="card">
				<div class="card-body">
					<div class="m-r-20 align-self-center"><span><i class="fa fa-lock f-s-40 color-danger"></i></span></div>
					<div class="align-self-center">
						<h6 class="text-muted m-t-0 m-b-0">Total de Jogadores Banidos</h6>
						<h4 class="m-t-0">{{$server->banned_players}}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="card">
				<div class="card-body">
					<div class="m-r-20 align-self-center"><span><i class="fa fa-plug f-s-40 color-info"></i></span></div>
					<div class="align-self-cente">
						<h6 class="text-muted m-t-0 m-b-0">IP</h6>
						<h4 class="m-t-0">{{$server->ip}}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="card">
				<div class="card-body">
					<div class="m-r-20 align-self-center"><span><i class="fa fa-cubes f-s-40 color-primary"></i></span></div>
					<div class="align-self-center">
						<h6 class="text-muted m-t-0 m-b-0">Build Atual</h6>
						<h4 class="m-t-0" id="actual_build">{{$server->build}}</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="card">
				<div class="card-body">
					<div class="m-r-20 align-self-center"><span><i class="fa fa-puzzle-piece f-s-40 color-info"></i></span></div>
					<div class="align-self-center">
						<h6 class="text-muted m-t-0 m-b-0">Plugin Size</h6>
						<h4 class="m-t-0">{{$server->plugin_size}}</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="d-flex no-block">
						<div>
							<h4 class="card-title"><span class="lstick"></span>Lista de usuarios</h4></div>
						</div>
						<div class="table-responsive">
							<table class="table vm no-th-brd" id="myTable">
								<thead>
									<tr>
										<th>Avatar</th>
										<th>Username</th>
										<th>UUID</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($users as $user)
										<tr>
											<td style="width:50px;">
												<img src="https://mc-heads.net/head/{{$user->UUID}}.png" style="width: 40px;">
											</td>
											<td>
												<h6>{{$user->username}}</h6>
											</td>
											<td>
												<h6>{{$user->UUID}}</h6>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card bg-burple text-white">
						<div class="card-body">
							<i class="fab fa-discord fa-2x text-white"></i>
							@if($server->discord_id == 0) 
								<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Adicionar ID</h4>
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											</div>
											<div class="modal-body">
												<form action="{{route('server.update', $server->id)}}" method="post">
													@csrf
													@method('put')
													<div class="form-group">
														<label for="discord_id">Discord ID</label>
														<input id="discord_id" type="number" class="form-control{{ $errors->has('discord_id') ? ' is-invalid' : '' }}" name="discord_id" required>
														@if ($errors->has('discord_id'))
															<span class="invalid-feedback" role="alert">
																<strong>{{ $errors->first('discord_id') }}</strong>
															</span>
														@endif
													</div>
													<div class="col-md-6 offset-md-4">
															<button type="submit" class="btn btn-primary">
																{{ __('Registrar') }}
															</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								<button type="button" class="btn waves-effect waves-light btn-rounded btn-success" data-toggle="modal" data-target="#responsive-modal">Adicionar um servidor</button>
							@else
								<div id="discord_result">
									<p class="text-white">Status do Discord</p>
									<button type="button" class="btn btn-rounded btn-block btn-info" onclick="updateStatus('{{$server->discord_id}}', this)">Clique aqui para ver</button>
									<p class="text-white" id="total_members" style="display: none;">0 Membros online</p>
									<p class="text-white" id="server_name" style="display: none;">Nenhum nome</p>
								</div>
							@endif
						</div>
					</div>
				</div>
		</div>
	
@endsection
@section('scripts')
	<script src="{{asset('js/lib/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js')}}"></script> 
	<script>
        $(document).ready( function () {
            $('#myTable').DataTable();
			
        });
		var str = document.getElementById('actual_build').innerText;
		var result = str.split("MC")[1].replace(/[-+():\s]/g, '');;
		document.getElementById("actual_build").innerHTML = result;

		function updateStatus(id_disco, ele) {
			$(ele).hide();
			const apiUrl = "https://discordapp.com/api/guilds/"+id_disco+"/widget.json";
			$.get(apiUrl, function(data, status) {
				try {
					const members = Array.isArray(data.members) ? data.members : [];
					console.log(status);
					$('#total_members').text(members.length + ' Membros online');
					$('#server_name').text(data.name);
				} catch(e) {
					$('#total_members').text("Não foi possivel fazer essa solicitação. Tente ir nas configurações do seu servidor no discord e habilite a opção Widget");

				}
			});
			$('#total_members').show();
			$('#server_name').show();
		}
	</script>
@endsection